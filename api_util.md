#Utility API

Here are some utility API.

##Get MD5 string
To get an MD5 string from a clear text you can call ```md5``` method.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /util/md5
```

**PARAMETERS**

- _app_token_ : Your access token
- _value_ : Value you want convert.


Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&value=VALUE-TO-CONVERT" 
-X POST https://api.botfarmy.com:4000/api/util/md5
```

Sample response:

```
{
  "response": "4A46FD452BDBE00C7B9603B1EB8866DD"
}
```
