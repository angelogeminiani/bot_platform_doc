#Blocks API

##Get Bot Block
This method allow you to retrieve Bot response block.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/get_bot_block
```

**PARAMETERS**
- _app_token_ : Your access token
- _key_ : KEY of the Block.

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&key=1ae5b0de77f54474ac6a93e6ba3a3924" 
-X POST https://api.botfarmy.com:4000/api/builder/get_bot_block
```

Sample response:

```
{
  "response": 
    {
      "_key": "1ae5b0de77f54474ac6a93e6ba3a3924",
      "bot_key": "bot_d95b875a748b4db49ca0b7b37b8212f3",
      "name": "subscriptions_off",
      "description": "subscriptions_off",
      "body": [
        {
          "elements": [],
          "media": "",
          "text": "<%msg_subscriptions_off%>"
        },
        "_subscriptions"
      ],
      "type": "type_block",
      "group": "blocks"
    }
}
```

##Update Bot Block
This method allow you to Update Bot Block.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/upsert_bot_block
```

**PARAMETERS**
- _app_token_ : Your access token
- _item_ : Json item (remember to encode string value using [encodeURIComponent()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/encodeURIComponent)).

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&item=JSON-TEXT" 
-X POST https://api.botfarmy.com:4000/api/builder/upsert_bot_block
```

Sample response:

```
{
  "response": 
    {
      "_key": "1ae5b0de77f54474ac6a93e6ba3a3924",
      "bot_key": "bot_d95b875a748b4db49ca0b7b37b8212f3",
      "name": "subscriptions_off",
      "description": "subscriptions_off",
      "body": [
        {
          "elements": [],
          "media": "",
          "text": "<%msg_subscriptions_off%>"
        },
        "_subscriptions"
      ],
      "type": "type_block",
      "group": "blocks"
    }
}
```

##Rename Bot Block
This method allow you to retrieve Bot response block.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/rename_bot_block
```

**PARAMETERS**
- _app_token_ : Your access token
- _key_ : KEY of the Block.
- _name_ : New name

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&key=1ae5b0de77f54474ac6a93e6ba3a3924&name=new_name" 
-X POST https://api.botfarmy.com:4000/api/builder/rename_bot_block
```

Sample response:

```
{
  "response": 
    {
      "_key": "1ae5b0de77f54474ac6a93e6ba3a3924",
      "bot_key": "bot_d95b875a748b4db49ca0b7b37b8212f3",
      "name": "new_name",
      "description": "subscriptions_off",
      "body": [
        {
          "elements": [],
          "media": "",
          "text": "<%msg_subscriptions_off%>"
        },
        "_subscriptions"
      ],
      "type": "type_block",
      "group": "blocks"
    }
}
```

##Remove Bot Block
This method allow you to remove Bot response block.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/remove_bot_block
```

**PARAMETERS**
- _app_token_ : Your access token
- _key_ : KEY of the Block.

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&key=1ae5b0de77f54474ac6a93e6ba3a3924" 
-X POST https://api.botfarmy.com:4000/api/builder/remove_bot_block
```

Sample response:

```
{
  "response": true
}
```

##Create Bot Block
This method allow you to create Bot response block.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/new_bot_block
```

**PARAMETERS**

- _app_token_ : Your access token
- _bot_id_ : KEY of the bot.
- _group_id_ : KEY of the group.


Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&bot_id=bot_d95b875a748b4db49ca0b7b37b8212f3&group_id=1234" 
-X POST https://api.botfarmy.com:4000/api/builder/new_bot_block
```

Sample response:

```
{
  "response": 
    {
      "_key": "1ae5b0de77f54474ac6a93e6ba3a3924",
      "bot_key": "bot_d95b875a748b4db49ca0b7b37b8212f3",
      "name": "subscriptions_off",
      "description": "subscriptions_off",
      "body": [
      ],
      "type": "type_block",
      "group_id": "1234"
    }
}
```

##Get Bot Blocks
This method allow you to retrieve Bot response blocks.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/get_bot_blocks
```

**PARAMETERS**
- _app_token_ : Your access token
- _bot_id_ : KEY of the Bot.
- _group_id_ : (Optional) Group filter

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&bot_id=bot_d95b875a748b4db49ca0b7b37b8212f3" 
-X POST https://api.botfarmy.com:4000/api/builder/get_bot_blocks
```

Sample response:

```
{
  "response": [
    {
      "_key": "1ae5b0de77f54474ac6a93e6ba3a3924",
      "bot_key": "bot_d95b875a748b4db49ca0b7b37b8212f3",
      "name": "subscriptions_off",
      "description": "subscriptions_off",
      "body": [
        {
          "elements": [],
          "media": "",
          "text": "<%msg_subscriptions_off%>"
        },
        "_subscriptions"
      ],
      "type": "type_block",
      "group": "blocks"
    }
    ...
  ]
}
```

# Create Block Entities
Each block can contain (property is 'body') one or more entities.
An Entity is an object with a 'type' attribute.

Supported types are:

- text
- image
- video
- audio
- card
- list
- quick_buttons
- buttons

##Create a Block Entity
This method allow you to create Bot Block Entity.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/new_bot_block_entity
```

**PARAMETERS**
- _app_token_ : Your access token
- _type_: One of supported entity types
- _key_ : (Optional) KEY of the Block. If a BLOCK KEY is specified, the block is updated with this 
new type and updated block schema is returned, otherwise is returned only the entity schema.

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&key=1ae5b0de77f54474ac6a93e6ba3a3924" 
-X POST https://api.botfarmy.com:4000/api/builder/new_bot_block_entity
```

Sample response (with bot key specified):

```
{
  "response": 
    {
      "_key": "1ae5b0de77f54474ac6a93e6ba3a3924",
      "bot_key": "bot_d95b875a748b4db49ca0b7b37b8212f3",
      "name": "subscriptions_off",
      "description": "subscriptions_off",
      "body": [
        {
          "elements": [],
          "media": "",
          "text": "<%msg_subscriptions_off%>"
        },
        "_subscriptions"
      ],
      "type": "type_block",
      "group": "blocks"
    }
}
```

Sample response (without bot key specified):

```
{
  "response": 
    {
        "type": "text",
        "text":"Hello, I'm a simple text"
    }
}
```

## Entity Types

### text
```
{
    "type": "text",
    "text":"Hello, I'm a simple text"
}
```

### image

### video

### audio

### card

### list

### quick_buttons

### buttons