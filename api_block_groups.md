#Block's Group API

##Get Single Group
This method allow you to retrieve Bot Block Group by its key.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/get_bot_block_group
```

**PARAMETERS**
- _app_token_ : Your access token
- _key_ : KEY of the Block.

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&key=1ae5b0de77f54474ac6a93e6ba3a3924" 
-X POST https://api.botfarmy.com:4000/api/builder/get_bot_block_group
```

Sample response:

```
{
  "response": 
    {
      "_key": "1ae5b0de77f54474ac6a93e6ba3a3924",
      "bot_key": "bot_d95b875a748b4db49ca0b7b37b8212f3",
      "name": "group_1",
      "description": "Sample group"
    }
}
```

##Rename Single Group
This method allow you to retrieve Bot Block Group by its key.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/rename_bot_block_group
```

**PARAMETERS**
- _app_token_ : Your access token
- _key_ : KEY of the Block.

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&key=1ae5b0de77f54474ac6a93e6ba3a3924&name=new_name" 
-X POST https://api.botfarmy.com:4000/api/builder/rename_bot_block_group
```

Sample response:

```
{
  "response": 
    {
      "_key": "1ae5b0de77f54474ac6a93e6ba3a3924",
      "bot_key": "bot_d95b875a748b4db49ca0b7b37b8212f3",
      "name": "new_name",
      "description": "Sample group"
    }
}
```

##Remove Single Group
This method allow you to remove Bot Block Group by its key.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/remove_bot_block_group
```

**PARAMETERS**
- _app_token_ : Your access token
- _key_ : KEY of the Block.

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&key=1ae5b0de77f54474ac6a93e6ba3a3924" 
-X POST https://api.botfarmy.com:4000/api/builder/remove_bot_block_group
```

Sample response:

```
{
  "response": true
}
```

##Create Single Group
This method allow you to create Bot Block Group.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/new_bot_block_group
```

**PARAMETERS**
- _app_token_ : Your access token
- _bot_id_ : KEY of the Bot.

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&bot_id=bot_d95b875a748b4db49ca0b7b37b8212f3" 
-X POST https://api.botfarmy.com:4000/api/builder/new_bot_block_group
```

Sample response:

```
{
  "response": 
    {
      "_key": "1ae5b0de77f54474ac6a93e6ba3a3924",
      "bot_key": "bot_d95b875a748b4db49ca0b7b37b8212f3",
      "name": "new_group_25t5",
      "description": "New Group"
    }
}
```

###Get Bot Block Groups
This method allow you to retrieve Groups by Bot key.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/get_bot_block_groups
```

**PARAMETERS**
- _app_token_ : Your access token
- _bot_id_ : KEY of the Bot.

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&bot_id=bot_d95b875a748b4db49ca0b7b37b8212f3" 
-X POST https://api.botfarmy.com:4000/api/builder/get_bot_block_groups
```

Sample response:

```
{
  "response": [
    {
      "_key": "1ae5b0de77f54474ac6a93e6ba3a3924",
      "bot_key": "bot_d95b875a748b4db49ca0b7b37b8212f3",
      "name": "group_1",
      "description": "Sample group"
    }
    ...
  ]
}
```

