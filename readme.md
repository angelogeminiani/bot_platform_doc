#API Documentation

Platform API are RESTful API.

To test API you can use [CURL](https://github.com/curl/curl), or if you prefer a graphic tool I'ld suggest to use [Postman](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop) extension for Google Chrome.
Postman is quite easy to use and can store your commands and organize them in groups.

**Note about request type:**
The API support this kind of POST requests:

- x-www-form-urlencoded

##Documentation Conventions

###Endpoint shortcut

API **enpoint** is:

```
https://api.botfarmy.com:4000/api
```

For brevity in documentation this path may be omitted and you will find the url in a short format like

```
/version
```
instead of 

```
https://api.botfarmy.com:4000/api/version
```

### Token placeholder

Most API require a personal Access Token for security reasons.

In this documentation you find a placeholder that must be replaced with your own token.

```
YOUR_ACCESS_TOKEN
```

For example

```
/accounts/get/YOUR_ACCESS_TOKEN/<account_id>
```

###Empty Response

When the API has nothing to return this is the response:

```
{
  "response": ""
}
```

###Error Response

When the API encounter an error this is the response:

```
{
  "error": "Error Message Here!!!"
}
```

##Test the Connection
First of all check connection to our server calling:

```
/version
```

This request does not need an access token and you should see a response like this:

```
 {"app":"1.0.1"}
```

Test it now clicking [here](https://api.botfarmy.com:4000/api/version)!

##AVAILABLE API

- [UTILITY](./api_util.md): Some utility API
- Bot Builder
    - [Block Groups](./api_block_groups.md) Manage Block Groups 
    - [Blocks](./api_blocks.md) Manage Blocks



##Builder API

In this chapter you'll learn how to use user Account (named Account) and Builder API to:

- Create/Update User Account
- Create/Update Bots

###Quick Start
To start call API you need an **Access Token** (contact Botfarmy to get your token).

Once you have the token you can start calling API.


###Account API
Account API allow you:

- Create new Account
- Update existing Account

####Get an Account
This method allow to retrieve an Account entity from a valid account key.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /account/get
```

**PARAMETERS**

- _app_token_ : Your access token
- _key_ : Account id. Every account has a unique attribute named "key".

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&key=a1e68784-2745-41b6-9d46-3f2653136e7" 
-X POST https://api.botfarmy.com:4000/api/accoun/get
```

Sample response:

```
{
  "response": {
    "_rev": "_UOIiVhe---",
    "last_name": "Geminiani",
    "_id": "accounts/a1e68784-2745-41b6-9d46-3f2653136e7d",
    "_key": "a1e68784-2745-41b6-9d46-3f2653136e7d",
    "first_name": "Angelo",
    "facebook_id": "1234"
  }
}
```


####Create/Update an Account
This method allow to create or update an Account using a Facebook ID (required account field).

**METHOD**
```
POST
```
**ENDPOINT**

```
 /account/upsert
```

**PARAMETERS**

- _app_token_ : Your access token
- _item_ : Json item (remember to encode string value using [encodeURIComponent()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/encodeURIComponent)).

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&item={'first_name':'Angelo', 'last_name':'Geminiani', 'facebook_id':'1234'}" 
-X POST https://api.botfarmy.com:4000/api/account/upsert
```

Sample response:

```
{
  "response": {
    "_rev": "_UOIiVhe---",
    "last_name": "Geminiani",
    "_id": "accounts/a1e68784-2745-41b6-9d46-3f2653136e7d",
    "_key": "a1e68784-2745-41b6-9d46-3f2653136e7d",
    "first_name": "Angelo",
    "facebook_id": "1234"
  }
}
```

####Get an Account by Facebook ID
[Facebook Login](https://developers.facebook.com/docs/facebook-login)
allow you to retrieve some data from facebook that can be saved into user Account.

This method allow you to get an Account searching for Facebook ID.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /account/get_by_facebook_id
```

**PARAMETERS**

- _app_token_ : Your access token
- _fb_id_ : The Facebook ID.

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&fb_id=FACEBOOK_ID" 
-X POST https://api.botfarmy.com:4000/api/account/get_by_facebook_id
```

Sample response:

```
{
  "response": {
        "_key": "a1e68784-2745-41b6-9d46-3f2653136e7d",
        "facebook_id": "FACEBOOK_ID",
        "facebook_token": "VALID_TOKEN",
        "first_name": "Gian Angelo",
        "last_name": "Geminani",
        "lang": "it",
        "profile_pic": "https://scontent.xx.fbcdn.net/v/t1.0-1/some_image.jpg",
        ......
        
  }
}
```

###Settings API

Settings API allow you to:

- Start/Stop the bot (a stopped bot does not respond to users)
- Enable/Disable a chat (disabled chat allow a human to interact with a customer pausing the bot)
- Get active chats

#### Start/Stop the bot.

You may need to stop the bot for all chats if you have humans that interact with customers chats.


**METHOD**
```
POST
```
**ENDPOINT**

```
 /bot_settings/off
```
```
 /bot_settings/on
```

**PARAMETERS**

- _app_token_ : Your access token
- _bot_id_ : ID of the bot

Sample request to STOP the bot:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&bot_id=BOT_ID" 
-X POST https://api.botfarmy.com:4000/api/bot_settings/off
```

Sample response:

```
{
  "response": [
    "*"
  ]
}
```
The response is an ARRAY of paused chat id. "**Asterisk**" means that all chats are paused.

#### Start/Stop a single chat.

This method allow you to stop a single chat. The bot does not respond to a specific chat.


**METHOD**
```
POST
```
**ENDPOINT**

```
 /bot_settings/off
```
```
 /bot_settings/on
```

**PARAMETERS**

- _app_token_ : Your access token
- _bot_id_ : ID of the bot
- _chat_id_: ID of chat you want pause

Sample request to STOP the chat:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&bot_id=BOT_ID&chat_id=ID_OF_CHAT" 
-X POST https://api.botfarmy.com:4000/api/bot_settings/off
```

Sample response:

```
{
  "response": [
    "ID_OF_CHAT"
  ]
}
```

#### Get active chats.

This method allow you retrieve an ARRAY of active chats.
The response ARRAY contains IDs of active chats/users.
Use ```/bot_user/get``` to retrieve the profile of current user in chat.


**METHOD**
```
POST
```
**ENDPOINT**

```
 /bot_settings/get_actives
```

**PARAMETERS**

- _app_token_ : Your access token
- _bot_id_ : ID of the bot


Sample request:
```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&bot_id=BOT_ID" 
-X POST https://api.botfarmy.com:4000/api/bot_settings/get_actives
```

Sample response:

```
{
  "response": [
    "ID_OF_ACTIVE_CHAT"
  ]
}
```

The response is an ARRAY of active chats with users. Each ID in ARRAY is a User ID, so you can get a USER PROFILE fo each ID.

#### Get user profile.

This method allow you retrieve a USER PROFILE.


**METHOD**
```
POST
```
**ENDPOINT**

```
 /bot_user/get
```

**PARAMETERS**

- _app_token_ : Your access token
- _bot_id_ : ID of the bot
- _user_id_ : ID of the user


Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&bot_id=BOT_ID&user_id=USER_ID" 
-X POST https://api.botfarmy.com:4000/api/bot_user/get
```

Sample response:

```
{
  "response": {
    "_key": "b9ee5f12f42d4d6dbf346bdc3cd537a2",
    "api_id": "364488933882846",
    "messenger_id": "364488933882846",
    "telegram_id": "364488933882846",
    "web_id": "undefined_75e45fca-78bf-4977-932d-df34acd31821",
    "first_name": "Mario",
    "last_name": "Rossi",
    "gender": "m",
    "timezone": "1",
    "profile_pic": "",
    "locale": "en",
    "lang": "en"
  }
}
```

###Dashboard API

Dashboard API allow you:

- Create new Bots
- Destroy a Bot
- Clone a Bot
- Update a Bot
- Deploy/Publish a Bot

What you **can not do** with API:

- Connect the Bot to Facebook (required web interaction with FB API to obtain Facebook Token).

####Create new Bot from scratch
Yuo have 2 distinct way to create a Bot:
- from scratch
- from a template

Create a Bot from scratch is usually the best way if you want create a template or an empty Bot.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/create_bot
```

**PARAMETERS**

- _app_token_ : Your access token
- _owner_id_ : KEY of the Bot Owner (an Account Key). Usually this is the ID (_key) of current logged-in user.
- _name_ : Bot Name. This field is required and must be a String shorter than 25 chars.
- _description_ : (Optional) A decription for this Bot. Omit if you want assign a description later.

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&owner_id=qaws-334455-ssxxds&name=a_simple_name" 
-X POST https://api.botfarmy.com:4000/api/builder/create_bot
```

Sample response:

```
{
  "response": {
    "_key": "bot_3da0022fd9004ca99e8165e9b16f795f",
    "bot_id": "bot_3da0022fd9004ca99e8165e9b16f795f",
    "is_template": false,
    "owner_id": "c71eaf9b-a71a-4f02-a9ff-6ff43aebb3e4",
    "name": "TEST_NEW_BOT",
    "description": "",
    "encoding": "UTF-8",
    "version": "1.0.0",
    "path": "public/bot_3da0022fd9004ca99e8165e9b16f795f",
    "uid": {
      "default": "bot_3da0022fd9004ca99e8165e9b16f795f",
      "channel_telegram": "",
      "channel_messenger": ""
    },
    "tokens": {
      "default": "undefined",
      "channel_telegram": "",
      "channel_messenger": ""
    },
    "config": {
      "welcome": {
        "default": "",
        "channel_api": "undefined",
        "channel_telegram": "undefined",
        "channel_messenger": "undefined"
      }
    },
    "session_timeout": 120000
  }
}
```

####Create new Bot from template
This method allow you to create a Bot starting from a template.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/create_bot
```

**PARAMETERS**

- _app_token_ : Your access token
- _owner_id_ : KEY of the Bot Owner (an Account Key). Usually this is the ID (_key) of current logged-in user.
- _template_id_ : KEY of a template. If the template does not exists, you will receive an error
- _name_ : Bot Name. This field is required and must be a String shorter than 25 chars.
- _description_ : (Optional) A decription for this Bot. Omit if you want assign a description later.

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&owner_id=qaws-334455-ssxxds&name=a_simple_name&template_id=THIS_IS_A_TEMPLATE_KEY" 
-X POST https://api.botfarmy.com:4000/api/builder/create_bot
```

Sample response:

```
{
  "response": {
    "_key": "bot_3da0022fd9004ca99e8165e9b16f795f",
    "bot_id": "bot_3da0022fd9004ca99e8165e9b16f795f",
    "is_template": false,
    "owner_id": "c71eaf9b-a71a-4f02-a9ff-6ff43aebb3e4",
    "name": "TEST_NEW_BOT",
    "description": "",
    "encoding": "UTF-8",
    "version": "1.0.0",
    "path": "public/bot_3da0022fd9004ca99e8165e9b16f795f",
    "uid": {
      "default": "bot_3da0022fd9004ca99e8165e9b16f795f",
      "channel_telegram": "",
      "channel_messenger": ""
    },
    "tokens": {
      "default": "undefined",
      "channel_telegram": "",
      "channel_messenger": ""
    },
    "config": {
      "welcome": {
        "default": "",
        "channel_api": "undefined",
        "channel_telegram": "undefined",
        "channel_messenger": "undefined"
      }
    },
    "session_timeout": 120000
  }
}
```

Sample error response (missing params):

```
{
  "error": "Bad Request, missing some parameters: owner_id,name,template_id"
}
```
Sample error response (template not found):
```
{
  "error": "Source not found: this_template_does_not_exists"
}
```

####Get a Bot
This method allow you to retrieve a Bot header entity.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/get_bot
```

**PARAMETERS**
- _app_token_ : Your access token
- _bot_id_ : KEY of the Bot.

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&bot_id=THIS_IS_A_BOT_KEY" 
-X POST https://api.botfarmy.com:4000/api/builder/get_bot
```

Sample response:

```
{
  "response": {
    "_key": "bot_3da0022fd9004ca99e8165e9b16f795f",
    "bot_id": "bot_3da0022fd9004ca99e8165e9b16f795f",
    "is_template": false,
    "owner_id": "c71eaf9b-a71a-4f02-a9ff-6ff43aebb3e4",
    "name": "TEST_NEW_BOT",
    "description": "",
    "encoding": "UTF-8",
    "version": "1.0.0",
    "path": "public/bot_3da0022fd9004ca99e8165e9b16f795f",
    "uid": {
      "default": "bot_3da0022fd9004ca99e8165e9b16f795f",
      "channel_telegram": "",
      "channel_messenger": ""
    },
    "tokens": {
      "default": "undefined",
      "channel_telegram": "",
      "channel_messenger": ""
    },
    "config": {
      "welcome": {
        "default": "",
        "channel_api": "undefined",
        "channel_telegram": "undefined",
        "channel_messenger": "undefined"
      }
    },
    "session_timeout": 120000
  }
}
```

####Remove a Bot
This method allow you to remove a Bot.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/remove_bot
```

**PARAMETERS**
- _app_token_ : Your access token
- _bot_id_ : KEY of the Bot.
- _owner_id_: KEY of bot owner

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&bot_id=THIS_IS_A_BOT_KEY&owner_id=OWNER_ID" 
-X POST https://api.botfarmy.com:4000/api/builder/remove_bot
```

Sample response:

```
{
  "response": {
    "_key": "bot_3da0022fd9004ca99e8165e9b16f795f",
    "bot_id": "bot_3da0022fd9004ca99e8165e9b16f795f",
    "is_template": false,
    "owner_id": "c71eaf9b-a71a-4f02-a9ff-6ff43aebb3e4",
    "name": "TEST_NEW_BOT",
    "description": "",
    "encoding": "UTF-8",
    "version": "1.0.0",
    "path": "public/bot_3da0022fd9004ca99e8165e9b16f795f",
    "uid": {
      "default": "bot_3da0022fd9004ca99e8165e9b16f795f",
      "channel_telegram": "",
      "channel_messenger": ""
    },
    "tokens": {
      "default": "undefined",
      "channel_telegram": "",
      "channel_messenger": ""
    },
    "config": {
      "welcome": {
        "default": "",
        "channel_api": "undefined",
        "channel_telegram": "undefined",
        "channel_messenger": "undefined"
      }
    },
    "session_timeout": 120000
  }
}
```

####Update a Bot
This method allow you to update a Bot header entity.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/upsert_bot
```

**PARAMETERS**
- _app_token_ : Your access token
- _item_ : Json String with bot entity.

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&item=THIS_IS_A_BOT_ENTITY" 
-X POST https://api.botfarmy.com:4000/api/builder/upsert_bot
```

Sample response:

```
{
  "response": {
    "_key": "bot_3da0022fd9004ca99e8165e9b16f795f",
    "bot_id": "bot_3da0022fd9004ca99e8165e9b16f795f",
    "is_template": false,
    "owner_id": "c71eaf9b-a71a-4f02-a9ff-6ff43aebb3e4",
    "name": "TEST_NEW_BOT",
    "description": "",
    "encoding": "UTF-8",
    "version": "1.0.0",
    "path": "public/bot_3da0022fd9004ca99e8165e9b16f795f",
    "uid": {
      "default": "bot_3da0022fd9004ca99e8165e9b16f795f",
      "channel_telegram": "",
      "channel_messenger": ""
    },
    "tokens": {
      "default": "undefined",
      "channel_telegram": "",
      "channel_messenger": ""
    },
    "config": {
      "welcome": {
        "default": "",
        "channel_api": "undefined",
        "channel_telegram": "undefined",
        "channel_messenger": "undefined"
      }
    },
    "session_timeout": 120000
  }
}
```

####Get all Bots by Owner 
This method allow you to retrieve all Bots of a particular user.
The response is an ARRAY of Bot entities.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/find_bot_by_owner
```

**PARAMETERS**
- _app_token_ : Your access token
- _owner_id_ : KEY of the Bot.

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&owner_id=OWNER_ID" 
-X POST https://api.botfarmy.com:4000/api/builder/find_bot_by_owner
```

Sample response:
```
{
  "response": [
    {
      "_key": "bot_d95b875a748b4db49ca0b7b37b8212f3",
      "bot_id": "bot_d95b875a748b4db49ca0b7b37b8212f3",
      "is_template": false,
      "owner_id": "angelo_geminiani",
      "name": "new bot",
      "description": "this is a new bot",
      "encoding": "UTF-8",
      "version": "1.0.0",
      "path": "public/bot_d95b875a748b4db49ca0b7b37b8212f3",
      "uid": {
        "default": "bot_d95b875a748b4db49ca0b7b37b8212f3",
        "channel_telegram": "",
        "channel_messenger": ""
      },
      "tokens": {
        "default": "undefined",
        "channel_telegram": "",
        "channel_messenger": ""
      },
      "config": {
        },
        "menu": {
          "default": [],
          "channel_telegram": [],
          "channel_messenger": [
            {
              "title": "Powered by Botfarmy",
              "type": "web_url",
              "url": "http://www.botfarmy.com/"
            }
          ]
        },
        "welcome": {
          "default": "<%msg_welcome%>"
        }
      },
      "session_timeout": 120000
    }
  ]
}
```

####Get all Templates
This method allow you to retrieve all Templates .
The response is an ARRAY of Bot entities.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/find_templates
```

**PARAMETERS**
- _app_token_ : Your access token

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN" 
-X POST https://api.botfarmy.com:4000/api/builder/find_templates
```

Sample response:
```
{
  "response": [
    {
      "_key": "bot_d95b875a748b4db49ca0b7b37b8212f3",
      "bot_id": "bot_d95b875a748b4db49ca0b7b37b8212f3",
      "is_template": true,
      "owner_id": "angelo_geminiani",
      "name": "new bot",
      "description": "this is a new bot",
      "encoding": "UTF-8",
      "version": "1.0.0",
      "path": "public/bot_d95b875a748b4db49ca0b7b37b8212f3",
      "uid": {
        "default": "bot_d95b875a748b4db49ca0b7b37b8212f3",
        "channel_telegram": "",
        "channel_messenger": ""
      },
      "tokens": {
        "default": "undefined",
        "channel_telegram": "",
        "channel_messenger": ""
      },
      "config": {
        },
        "menu": {
          "default": [],
          "channel_telegram": [],
          "channel_messenger": [
            {
              "title": "Powered by Botfarmy",
              "type": "web_url",
              "url": "http://www.botfarmy.com/"
            }
          ]
        },
        "welcome": {
          "default": "<%msg_welcome%>"
        }
      },
      "session_timeout": 120000
    }
  ]
}
```


####Get Bot Languages
This method allow you to get all Bot languages.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/get_bot_languages
```

**PARAMETERS**
- _app_token_ : Your access token
- _bot_id_ : KEY of the Bot.

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&bot_id=THIS_IS_A_BOT_KEY" 
-X POST https://api.botfarmy.com:4000/api/builder/get_bot_languages
```

Sample response:
```
{
  "response": {
    {
      "response": [
        "default",
        "it",
        "en"
      ]
    }
  }
```

####Get a Bot's Localizations
This method allow you to retrieve a Bot localizations.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/get_bot_i18n
```

**PARAMETERS**
- _app_token_ : Your access token
- _bot_id_ : KEY of the Bot.

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&bot_id=THIS_IS_A_BOT_KEY" 
-X POST https://api.botfarmy.com:4000/api/builder/get_bot_i18n
```

Sample response:

```
{
  "response": {
    "default":{
       "say_hello": "Hola" 
    },
    "it": {
       "say_hello": "Ciao" 
    },
    "en": {
        "say_hello": "Hello"
    }
  }
```

####Update Bot's Localizations
This method allow you to update Bot localizations.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/upsert_bot_i18n
```

**PARAMETERS**
- _app_token_ : Your access token
- _bot_id_ : KEY of the Bot.
- _item_ : Json localization data (remember to encode string value using [encodeURIComponent()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/encodeURIComponent)).

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&bot_id=THIS_IS_A_BOT_KEY&item=THIS_IS_JSON_ENCODED_ITEM" 
-X POST https://api.botfarmy.com:4000/api/builder/upsert_bot_i18n
```

Sample response:
```
{
  "response": {
    "default":{
       "say_hello": "Hola" 
    },
    "it": {
       "say_hello": "Ciao" 
    },
    "en": {
        "say_hello": "Hello"
    }
  }
```

####Update Bot's single localized label
This method allow you to update Bot localization.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/upsert_bot_i18n
```

**PARAMETERS**
- _app_token_ : Your access token
- _bot_id_ : KEY of the Bot.
- _lang_ : Language to update
- _name_ : Name of the field to update
- _value_: New Value

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&bot_id=THIS_IS_A_BOT_KEY&lang=en&name=say_hello&value=SAY_HELLO_TO_ME" 
-X POST https://api.botfarmy.com:4000/api/builder/upsert_bot_i18n
```

Sample response:

```
{
  "response": {
    "_key": "3725bba3779a4477a36580f9cc64b6a1",
    "bot_key": "THIS_IS_A_BOT_KEY",
    "name": "say_hello",
    "lang": "en",
    "value": "SAY_HELLO_TO_ME"
  }
}
```

####Get Bot Keywords
This method allow you to retrieve Bot keywords for AI training.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/get_bot_keywords
```

**PARAMETERS**
- _app_token_ : Your access token
- _bot_id_ : KEY of the Bot.
- _lang_ : Language

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&bot_id=THIS_IS_A_BOT_KEY&lang=default" 
-X POST https://api.botfarmy.com:4000/api/builder/get_bot_keywords
```

Sample response:

```
{
  "response": [
    {
      "expression": [
        "(what|which) + (name|surname)"
      ],
      "pos": 0,
      "bot_key": "bot_d95b875a748b4db49ca0b7b37b8212f3",
      "_key": "681dccae460a444485d40c8afe9b4da0",
      "lang": "default",
      "value": "bot_my_name"
    },
    {
      "expression": [
        "(what|witch) + (job|work)"
        ],
        "pos": 1,
        "bot_key": "bot_d95b875a748b4db49ca0b7b37b8212f3",
        "_key": "80fc50a13c2646d2b42bcc6fcf18e72f",
        "lang": "default",
        "value": "summary"
    },
    {
      "expression": [
        "how + you",
        "are + (happy|sad|miserable)"
      ],
      "pos": 2,
      "bot_key": "bot_d95b875a748b4db49ca0b7b37b8212f3",
      "_key": "44349236eebc40b08da9a5dbac228fde",
      "lang": "default",
      "value": "bot_is_fine"
    },
    
    {
      "expression": [
        "(start|help|run|begin|go|hello|hi|hey)"
      ],
      "pos": 3,
      "bot_key": "bot_d95b875a748b4db49ca0b7b37b8212f3",
      "_key": "313228df7d3741baa0ad09f56987835e",
      "lang": "default",
      "value": "home"
    }
  ]
}
```
####Create Bot Keywords
This method allow you to create Bot keywords for AI training.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/new_bot_keyword
```

**PARAMETERS**
- _app_token_ : Your access token
- _bot_id_ : KEY of Bot.
- _lang_ : language
- _pos_ : position in list

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&bot_id=bot_d95b875a748b4db49ca0b7b37b8212f3&lang=en&pos=1" 
-X POST https://api.botfarmy.com:4000/api/builder/new_bot_keyword
```

Sample response:
```
{
  "response": {
    "_key": "44349236eebc40b08da9a5dbac228fde",
    "expression": [ ],
    "pos": 1,
    "bot_key": "bot_d95b875a748b4db49ca0b7b37b8212f3",
    "lang": "en",
    "value": ""
  }
}
```

####Update Bot Keywords
This method allow you to update Bot keywords for AI training.

Fields you can update:
- value: the intent
- lang: the language
- pos: position in list
- expression: expression for semantic engine

"value", "lang", "pos" and "expression" are Options. At least one is required.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/upsert_bot_keyword
```

**PARAMETERS**
- _app_token_ : Your access token
- _key_ : KEY of entity.
- _value_: (Option) new intent 
- _lang_ : (Option) language
- _pos_ : (Option) position in list
- _expression_ : (Options) ARRAY containing the semantic expression

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&key=44349236eebc40b08da9a5dbac228fde&value=bot_is_fine&lang=en&pos=0" 
-X POST https://api.botfarmy.com:4000/api/builder/upsert_bot_keyword
```

Sample response:
```
{
  "response": {
    "_key": "44349236eebc40b08da9a5dbac228fde",
    "expression": [
      "how + you",
      "are + (happy|sad|miserable)"
    ],
    "pos": 0,
    "bot_key": "bot_d95b875a748b4db49ca0b7b37b8212f3",
    "lang": "en",
    "value": "bot_is_fine"
  }
}
```

####Get Bot Domains
This method allow you to retrieve Bot domains for AI training.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/get_bot_domains
```

**PARAMETERS**
- _app_token_ : Your access token
- _bot_id_ : KEY of the Bot.
- _lang_ : (Optional) Language filter, if omitted the response contains all languages

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&bot_id=bot_d95b875a748b4db49ca0b7b37b8212f3&lang=default" 
-X POST https://api.botfarmy.com:4000/api/builder/get_bot_domains
```

Sample response:

```
{
      "response": [
        {
            "_key": "fc0f5de7e43f4b70a2837e140b96901f",
            "name": "search",
            "bot_key": "bot_d95b875a748b4db49ca0b7b37b8212f3",
            "lang": "default"
            "expression": [
                {
                  "keys": [
                    "(shoe|shoes|skeaker|sneakers)"
                  ],
                  "params": "f_family=shoe",
                  "value": ""
                },
                {
                  "keys": [
                    "(find|search|lookup|get|buy)"
                  ],
                  "value": "items?action=filter&mode=reset"
                }
            ]
        }
      ]
} 
```

####Create Bot Domains
This method allow you to create Bot domains for AI training.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/new_bot_domain
```

**PARAMETERS**
- _app_token_ : Your access token
- _bot_id_ : KEY of Bot.
- _lang_ : language
- _name_ : name of domain (unique for each lang)

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&bot_id=bot_d95b875a748b4db49ca0b7b37b8212f3&lang=en&name=search_store" 
-X POST https://api.botfarmy.com:4000/api/builder/new_bot_domain
```

Sample response:
```
{
  "response": {
    "_key": "fc0f5de7e43f4b70a2837e140b96901f",
    "expression": [],
    "name": "search_store",
    "bot_key": "bot_d95b875a748b4db49ca0b7b37b8212f3",
    "lang": "en"
  }
}
```

####Update Bot Domains
This method allow you to update Bot domains for AI training.

**METHOD**
```
POST
```
**ENDPOINT**

```
 /builder/upsert_bot_domain
```

**PARAMETERS**
- _app_token_ : Your access token
- _bot_id_ : KEY of the Bot.
- _lang_ : (Optional) Language filter, if omitted the response contains all languages

Sample request:

```bash
curl -d "app_token=YOUR_ACCESS_TOKEN&key=fc0f5de7e43f4b70a2837e140b96901f&lang=default" 
-X POST https://api.botfarmy.com:4000/api/builder/upsert_bot_domain
```

Sample response:

```
{
       "response": [
         {
             "_key": "fc0f5de7e43f4b70a2837e140b96901f",
             "name": "search",
             "bot_key": "bot_d95b875a748b4db49ca0b7b37b8212f3",
             "lang": "default"
             "expression": [
                 {
                   "keys": [
                     "(shoe|shoes|skeaker|sneakers)"
                   ],
                   "params": "f_family=shoe",
                   "value": ""
                 },
                 {
                   "keys": [
                     "(find|search|lookup|get|buy)"
                   ],
                   "value": "items?action=filter&mode=reset"
                 }
             ]
         }
       ]
 } 
```

